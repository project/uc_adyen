<?php

/**
 * @file
 * Adyen menu items.
 *
 */

/**
 * This function receives the customer back in the online shop. The here received information comes via the customer's browser and therefore backend data processing is in uc_adyen_notification.
 */
function uc_adyen_complete() {
  $merchant_reference = $_GET['merchantReference'];
  $skin_code          = $_GET['skinCode'];
  $shopper_locale     = $_GET['shopperLocale'];
  $payment_method     = $_GET['paymentMethod'];
  $auth_result        = $_GET['authResult'];
  $psp_reference      = $_GET['pspReference'];
  $merchant_sig       = $_GET['merchantSig'];

  // verify that data wasn't corrupted during transmission - exit otherwise
  $hmac_data  = $auth_result . $psp_reference . $merchant_reference . $skin_code;
  $verify_sig = base64_encode(hash_hmac('sha1', $hmac_data, variable_get('uc_adyen_shared_secret', ''), TRUE));
  if ($merchant_sig != $verify_sig) {
    watchdog('uc_adyen', 'Corrupt data in URL received from customer.', NULL, WATCHDOG_WARNING);
    // chances are the problem is FROM the customer's browser - still: below message makes it clearer that the payment was potentially successful
    drupal_set_message(t('There was an error with the transmission to your browser, you can verify your payment status in your account.'), 'error');
    drupal_goto('user');
  }

  // Create user message depending on Adyen result
  switch ($auth_result) {
    case 'AUTHORISED':
      drupal_set_message(t('Your payment has been confirmed.'), 'status');
      break;

    case 'PENDING':
      drupal_set_message(t('Your order will be processed as soon as your payment clears at Adyen.'), 'status');
      break;

    case 'REFUSED':
      drupal_set_message(t('Your payment has been refused.'), 'error');
      drupal_goto('cart');
      break;

    case 'ERROR':
      drupal_set_message(t('An error has occurred during payment. Please contact us to ensure your order has submitted.'), 'error');
      drupal_goto('cart');
      break;
  }

  $order = uc_order_load($merchant_reference);
  // the following command will among other things set order status to "Pending"
  // note: actual payment confirmation should only come directly from Adyen
  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

  // check if alternate checkout completion page is set
  $page = variable_get('uc_cart_checkout_complete_page', '');
  if (!empty($page)) {
    drupal_goto($page);
  }

  return $output;
}

/**
 * This function processes the payment outcome. The here received information comes directly from the Adyen server.
 */
function uc_adyen_notification($cart_id = 0) {
  $live                  = $_POST['live'];
  $event_code            = $_POST['eventCode'];
  $psp_reference         = $_POST['pspReference'];
  $original_reference    = $_POST['originalReference'];
  $merchant_reference    = $_POST['merchantReference'];
  $merchant_account_code = $_POST['merchantAccountCode'];
  $event_date            = $_POST['eventDate'];
  $success               = $_POST['success'];
  $payment_method        = $_POST['paymentMethod'];
  $operations            = $_POST['operations'];
  $reason                = $_POST['reason'];
  $currency              = $_POST['currency'];
  $value                 = $_POST['value'];

  // First log the received message and confirm reception to Adyen.com
  $id = db_insert('uc_payment_adyen_notifications')
  ->fields(array(
    'live' => $live,
    'event_code' => $event_code,
    'psp_reference' => $psp_reference,
    'original_reference' => $original_reference,
    'merchant_reference' => $merchant_reference,
    'merchant_account_code' => $merchant_account_code,
    'event_date' => $event_date,
    'success' => $success,
    'payment_method' => $payment_method,
    'operations' => $operations,
    'reasons' => $reason,
    'currency' => $currency,
    'value' => $value,
  ))
  ->execute();
  print('notificationResponse = [accepted]');

  // After(!) saving the data to the log we check validity

  // Check if redundant message - ignore in that case
  $count = db_query("SELECT COUNT(notification_id) FROM {uc_payment_adyen_notifications} WHERE event_code = :event_code AND psp_reference = :psp_reference", array(':event_code' => $event_code, ':psp_reference' => $psp_reference))->fetchField();
  if ($count > 1) {
    watchdog('uc_adyen', 'Ignoring redundant payment message.', NULL, WATCHDOG_INFO);
    exit();
  }

  // Check source IP within correct range: 82.199.90.136/29 OR 82.199.90.160/27 OR 91.212.42.0/24
  // TBD: the ranges used in this test are a bit too big, this could be improved. Even could be an admin-editable list.
  if (drupal_substr(ip_address(), 0, 11) != "82.199.90.1" && drupal_substr(ip_address(), 0, 10) != "91.212.42.") {
    $errors[] = t('Invalid remote IP @ip.', array('@ip' => ip_address()));
  }

  // Check order ID exists, if so check further details
  $order = uc_order_load($merchant_reference);
  if (!$order) {
    $errors[] = t('Invalid order @id.', array('@id' => $order->order_id));
  }
  else {
    // Since order exists we save the log entry also to the order
    uc_order_comment_save($merchant_reference, 0, $log_entry, 'admin');

    // Check merchant account
    if (variable_get('uc_adyen_merchant_account', '') != $merchant_account_code) {
      $errors[] = t('Incorrect merchant account: @merchant.', array('@merchant' => $merchant_account_code));
    }

    // Check status
    if ($order->order_status != 'in_checkout' &&
        $order->order_status != 'pending') {
      $errors[] = t('Order was neither in checkout nor pending.');
    }

    // Check price
    if (round($order->order_total, 2) != ($value / 100)) {
      $errors[] = t('Incorrect price: @price1 vs @price2.', array('@price1' => round($order->order_total, 2), '@price2' => ($value / 100)));
    }

    // Check currency
    if (variable_get('uc_currency_code', 'EUR') != $currency) {
      $errors[] = t('Incorrect currency code @currency.', array('@currency' => $currency));
    }
  }

  if (!empty($errors)) {
    // Log errors to order and watchdog.
    if ($order) {
      $error_message = 'Order ' . $merchant_reference . ' caused error: ' . implode('<br />', $errors);
      uc_order_comment_save($order->order_id, 0, $error_message, 'admin');
    }
    $error_data = array(
      '@id' => $merchant_reference,
      '@error' => implode('<br />', $errors),
    );
    watchdog('uc_adyen', 'Order @id caused error: @error', $error_data, WATCHDOG_ERROR);

    exit();
  }

  // Data ok, process now

  switch ($event_code) {
    case 'AUTHORISATION':
      if ($success == 'true') {
        // Empty cart - can be in test or live system
        uc_cart_empty($cart_id);
        if ($live == 'true') {
          // Sale complete - only the life system can confirm payments
          uc_order_update_status($order->order_id, 'payment_received');
          $comment = t('Paid by @method, Adyen.com reference @pspRef.', array('@method' => $payment_method, '@pspRef' => $psp_reference));
          uc_payment_enter($order->order_id, 'adyen', $order->order_total, 0, NULL, $comment);
          // Add a comment to let sales team know this came in through the site.
          uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');
          // And show some progress to the user.
          uc_order_comment_save($order->order_id, 0, t('Payment accepted by Adyen.'), 'order', 'payment_received');
        }
        else {
          // Sale complete - test system doesn't affect order status or balance
          $comment = t('TEST - TEST - Paid by @method, Adyen.com reference #@pspRef - TEST - TEST.', array('@method' => $payment_method, '@pspRef' => $psp_reference));
          uc_payment_enter($order->order_id, 'adyen', 0, 0, NULL, $comment);
          // Add a comment to let sales team know this came in through the site.
          uc_order_comment_save($order->order_id, 0, t('TEST - TEST - Order created through website. - TEST - TEST'), 'admin');
          // And show some progress to the user.
          uc_order_comment_save($order->order_id, 0, t('Test payment notification received from Adyen.'), 'order', 0);
        }
      }
      else {
        uc_order_comment_save($order->order_id, 0, t('Unsuccessful authorisation attempt.'), 'admin');
      }
      break;

    case 'CANCELLATION':
      uc_order_update_status($order->order_id, 'canceled');
      uc_order_comment_save($order->order_id, 0, t('Payment cancelled.'), 'admin');
      break;

    case 'REFUND':
    case 'CAPTURE':
    case 'REQUEST_FOR_INFORMATION':
    case 'NOTIFICATION_OF_CHARGEBACK':
    case 'ADVICE_OF_DEBIT':
    case 'CHARGEBACK':
      break;
  }

  return;
}

